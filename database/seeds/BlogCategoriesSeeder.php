<?php

use Illuminate\Database\Seeder;

/**
 * Class BlogCategoriesSeeder
 */
class BlogCategoriesSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        DB::table('blog_categories')->insert([
            'slug' => 'main',
            'title' => 'Главная'
        ]);
        DB::table('blog_categories')->insert([
            'slug' => 'loan',
            'title' => 'Займы'
        ]);
    }
}
