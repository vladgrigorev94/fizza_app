<?php

Route::group(['middleware' => ['web']], function () {
    Route::get('/', 'MainController@mainPage');
    Route::get('/loan', 'MainController@loanPage');
    Route::post('/create-request', 'MainController@createRequest');

  //  Route::group(['middleware' => ['auth']], function () {
        Route::get('/steps', 'MainController@steps');
   // });

    Route::post('/create-user', 'MainController@createUser');
    Route::get('/user', 'MainController@getUser');
    Route::put('/user', 'MainController@updateUser');
    Route::post('/upload-file', 'MainController@uploadFile');
    Route::get('/user-files', 'MainController@getUserFiles');
    Route::post('/delete-file', 'MainController@deleteUserFile');
    Route::get('/logout', 'MainController@logout');
    Route::post('/is-auth', 'MainController@isAuth');
    Route::post('/get-faqs', 'CommonController@getFaqs');
    Route::post('/get-reviews', 'CommonController@getReviews');
    Route::post('/get-blogs', 'CommonController@getBlogs');
    Route::post('/save-answer', 'MainController@saveAnswer');
    Route::get('/questions', 'MainController@getQuestions');

    Route::group(['prefix' => 'new-blog'], function () {
        Route::get('/', 'NewBlogController@listBlogs');
        Route::post('/getPosts',
            'NewBlogController@getPosts')
            ->name('getPosts');
        Route::post('/getLastPosts',
            'NewBlogController@getLastPosts')
            ->name('getLastPosts');
    });

    Auth::routes(['register' => false]);
});




