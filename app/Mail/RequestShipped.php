<?php

namespace App\Mail;

use App\Models\Request;
use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class RequestShipped extends Mailable
{
    use Queueable, SerializesModels;

    public $request;

    /**
     * OrderShipped constructor.
     * @param Request $request
     */
    public function __construct(Request $request)
    {
        $this->request = $request;
    }

    /**
     * @return $this
     */
    public function build()
    {
        return $this->view('email.request')
            ->from('mail@fizza.ru', 'Займы')
            ->subject('Поступила заяква на займ.')
            ->with([
                'requestModel' => $this->request
            ]);
    }

}
