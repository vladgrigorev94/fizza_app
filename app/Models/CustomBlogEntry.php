<?php

namespace App\Models;

use Bjuppa\LaravelBlog\Eloquent\BlogEntry;
use Bjuppa\LaravelBlog\Support\Author;
use Bjuppa\LaravelBlog\Support\MarkdownString;
use Bjuppa\LaravelBlog\Support\SummaryExtractor;
use Bjuppa\MetaTagBag\MetaTagBag;
use Illuminate\Contracts\Support\Htmlable;
use Illuminate\Support\Collection;
use Illuminate\Support\HtmlString;
use Spatie\Sluggable\SlugOptions;

/**
 * Class CustomBlogEntry
 * @package App\Models
 */
class CustomBlogEntry extends BlogEntry
{
    /**
     * Get the options for generating the slug.
     */
    public function getSlugOptions(): SlugOptions
    {
        $options = SlugOptions::create()
            ->generateSlugsFrom('title')
            ->saveSlugsTo($this->getRouteKeyName());

        if ($this->isPublic()) {
            $options->doNotGenerateSlugsOnUpdate();
        }

        return $options;
    }

    /**
     * Get the entry's headline
     * @return string
     */
    public function getTitle(): string
    {
        return $this->title;
    }

    /**
     * Get the entry's headline
     * @return string
     */
    public function getBlog(): string
    {
        return $this->blog;
    }

    /**
     * Get the entry's full body text with markup
     * @return Htmlable
     */
    public function getContent(): Htmlable
    {
        return new MarkdownString($this->content);
    }

    /**
     * The entry's authors
     * An empty collection indicates the entry should be considered written by the blog's default author
     * @return Collection
     */
    public function getAuthors(): Collection
    {
        $authors = collect();
        if ($this->author_name) {
            $authors->push(new Author([
                'name' => $this->author_name,
                'email' => $this->author_email,
                'url' => $this->author_url,
            ]));
        }
        return $authors;
    }

    /**
     * The entry's main image (if applicable), tagged in html
     * @return Htmlable|null
     */
    public function getImage(): ?Htmlable
    {
        if (empty($this->image)) {
            return null;
        }

return new HtmlString('<img width=100 src="' . e($this->image) . '" alt="" class="attachment-post-standard " />');
}

public
function getImageSrc(): ?string
    {
        if (empty($this->image)) {
            return null;
        }

        return $this->image;
    }

/**
 * Url for the entry's main image (if applicable)
 * @return string|null
 */
public function getImageUrl(): ?string
    {
        if (starts_with($this->image, ['http://', 'https://', '//'])) {
            return $this->image;
        }

        // Match quotes or opening parenthesis with matching end
        // Within that, capture http:// or https:// or just // and all following non-space characters into subpattern 3
        if (preg_match('/((\'|")|\()((https?:)?\/\/\S+).*(?(2)\2|\))/s', $this->image, $matches)) {
            return $matches[3];
        }

        return null;
    }


    public function getPartOfSummary(): Htmlable
    {
        if (!empty(trim($this->summary))) {
            return new MarkdownString($this->summary);
        }
        $substr = substr($this->content, 0, 500) . '...';

        return new HtmlString($substr);
    }

    public function getSummary(): Htmlable
    {
        if (!empty(trim($this->summary))) {
            return new MarkdownString($this->summary);
        }

        return new HtmlString($this->content);
    }

    /**
     * Get the meta-description for this entry
     * @return string|null
     */
    public function getMetaDescription(): ?string
    {
        return trim($this->description);
    }

    /**
     * Get the html head title for this entry
     * @param string $suffix to append after the title
     * @return string
     */
    public function getPageTitle(string $suffix = ''): string
{
    return str_finish($this->page_title ?? $this->getTitle(), $suffix);
}

    public function getJsonMetaTagsAttribute()
{
    return json_encode($this->meta_tags, JSON_PRETTY_PRINT);
}

    public function setJsonMetaTagsAttribute(?string $value)
    {
        $this->meta_tags = json_decode($value, true);
    }

    /**
     * Get any custom meta-tag attributes for this entry
     * @return MetaTagBag
     */
    public function getMetaTagBag(): MetaTagBag
{
    return MetaTagBag::make(
        ['property' => 'og:title', 'content' => $this->getTitle()],
        ['property' => 'og:type', 'content' => 'article'],
        ['property' => 'article:published_time', 'content' => $this->getPublished()->toIso8601String()],
        ['property' => 'article:modified_time', 'content' => $this->getUpdated()->toIso8601String()]
    )
        ->pipe(function ($bag) {
            if ($this->getMetaDescription()) {
                $bag->merge(['name' => 'description', 'content' => $this->getMetaDescription()]);
            }

            if ($this->getImageUrl()) {
                $bag->merge(
                    ['name' => 'twitter:card', 'content' => 'summary_large_image'],
                    ['name' => 'twitter:image', 'content' => $this->getImageUrl()]
                );
            }

            return $bag;
        })
        ->merge($this->meta_tags);
}

    /**
     * Check if complete entry contents should be made available in feed
     * @param bool $default
     * @return bool
     */
    public function displayFullContentInFeed(bool $default = false): bool
{
    return $this->display_full_content_in_feed ?? $default;
}
}
