<?php

namespace App\Models;

use App\Exceptions\Models\RequestException;
use App\Mail\RequestShipped;
use Illuminate\Database\Eloquent\Model;
use Backpack\CRUD\CrudTrait;
use Illuminate\Support\Facades\Mail;
use Illuminate\Http\Request;
/**
 * Class Question
 * @package App\Models
 */
class Question extends Model
{
    use CrudTrait;

    protected $table = 'questions';

    protected $fillable = [
        'user_id', 'text'
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function users()
    {
        return $this->belongsToMany(
            'App\User',
            'user_questions'
        );
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function userQuestion()
    {
        return $this->belongsTo('App\User');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function user()
    {
        return $this->belongsTo(
            'App\Models\UserQuestion'
        );
    }

    public function answers()
    {
        return $this->hasMany(
            'App\Models\UserQuestion'
        );
    }

    /**
     * @param Request $request
     */
    public static function saveAnswer(Request $request)
    {
        $question = Question::where(['id' => $request->questionId])
            ->first();
        $answers = $question->answers;
        $answer = $answers[0]??new UserQuestion();
        $answer->user_id = \Auth::user()->id;
        $answer->question_id = $request->questionId;
        $answer->answer = $request->answer;
        $answer->save();

    }

}
