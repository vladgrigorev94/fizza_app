<?php

namespace App\Models;

use App\Exceptions\Models\RequestException;
use App\Mail\RequestShipped;
use Illuminate\Database\Eloquent\Model;
use Backpack\CRUD\CrudTrait;
use Illuminate\Support\Facades\Mail;

/**
 * Class Faq
 * @package App\Models
 */
class Faq extends Model
{
    use CrudTrait;

    protected $table = 'faqs';

    protected $fillable = [
        'title', 'content', 'page_id'
    ];

    public function page()
    {
        return $this->belongsTo('App\Models\Page');
    }
}
