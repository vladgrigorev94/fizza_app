<?php

namespace App\Models;

use App\Exceptions\Models\RequestException;
use App\Mail\RequestShipped;
use Illuminate\Database\Eloquent\Model;
use Backpack\CRUD\CrudTrait;
use Illuminate\Support\Facades\Mail;

/**
 * Class Request
 * @package App\Models
 */
class Request extends Model
{
    use CrudTrait;

    protected $table = 'requests';
    // protected $primaryKey = 'id';
    // public $timestamps = false;
    // protected $guarded = ['id'];
    protected $fillable = [
        'inn', 'username', 'email',
        'number', 'address', 'city',
        'area', 'flats_count'];
    // protected $hidden = [];
    // protected $dates = [];

    /**
     * @param array $requestData
     * @throws RequestException
     */
    public static function makeRequest(array $requestData)
    {
        try {
            $requestModel = new self();
            foreach ($requestData as $key => $value) {
                $attributes = $requestModel->getFillable();
                if (in_array($key, $attributes)) {
                    $requestModel->$key = $value;
                }
            }
            $requestModel->save();
            Mail::to('mail@fizza.ru')
                ->queue(new RequestShipped($requestModel));
        } catch (\Exception $e) {
            throw new RequestException($e->getMessage());
        }
    }
}
