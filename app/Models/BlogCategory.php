<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Backpack\CRUD\CrudTrait;

/**
 * Class BlogCategory
 * @package App\Models
 */
class BlogCategory extends Model
{
    use CrudTrait;

    /*
    |--------------------------------------------------------------------------
    | GLOBAL VARIABLES
    |--------------------------------------------------------------------------
    */

    protected $table = 'blog_categories';
    // protected $primaryKey = 'id';
    // public $timestamps = false;
    // protected $guarded = ['id'];
    // protected $hidden = [];
    protected $dates = ['publish_after'];

    protected $fillable = [
        'slug',
        'title'
    ];

    /**
     * @return array
     */
    public static function getCategoriesForSelect()
    {
        $categories = self::all();
        $newCategories = [];
        foreach ($categories as $category) {
            $newCategories[$category->slug] = $category->title;
        }

        return $newCategories;
    }
}
