<?php

namespace App\Providers;

use App\Models\CustomBlogEntry;
use Facades\Bjuppa\LaravelBlog\Contracts\BlogRegistry;
use Illuminate\Support\ServiceProvider;
use Carbon\Carbon;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        Carbon::setLocale('ru');
        $blogs = BlogRegistry::all();
        foreach($blogs as $blog) {
            $blogEntryModel = new CustomBlogEntry();
            $blog->getEntryProvider()->withEntryModel($blogEntryModel);
        }
    }
}
