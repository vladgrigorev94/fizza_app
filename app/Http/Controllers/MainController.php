<?php

namespace App\Http\Controllers;

use App\Exceptions\Models\RequestException;
use App\Models\Question;
use App\Models\Request as RequestModel;
use App\Models\Review;
use App\User;
use Facades\Bjuppa\LaravelBlog\Contracts\BlogRegistry;
use Illuminate\Http\Request;

/**
 * Class MainController
 * @package App\Http\Controllers
 */
class MainController extends Controller
{

    /**
     * @return mixed
     */
    public function mainPage()
    {
        $blogs = BlogRegistry::all();

        return view('main.mainPage', [
            'blogs' => $blogs,
        ]);
    }

    /**
     * @return mixed
     */
    public function loanPage()
    {
        $reviews = Review::all();
        $blogs = BlogRegistry::all();

        return view('main.loanPage', [
            'reviews' => $reviews,
            'blogs' => $blogs,
        ]);
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     * @throws \App\Exceptions\Models\RequestException
     */
    public function createRequest(Request $request)
    {
        RequestModel::makeRequest($request->toArray());

        return redirect('/');
    }

    /**
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function steps(Request $request)
    {
        return view('main.steps');
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     * @throws RequestException
     */
    public function createUser(Request $request)
    {
        User::createUser($request);

        return '/second-step';
    }

    /**
     * @param Request $request
     * @return User
     */
    public function getUser(Request $request)
    {
        $user = User::getUser(\Auth::user()->id);

        return $user;
    }

    /**
     * @param Request $request
     * @throws RequestException
     */
    public function updateUser(Request $request)
    {
        $user = User::updateUser($request);

        return $user;
    }

    /**
     * @return mixed
     */
    public function getUserFiles()
    {
        $files = User::getUserFiles();

        return $files;
    }

    public function deleteUserFile(Request $request)
    {
        User::deleteUserFile($request);
    }

    /**
     * @param Request $request
     * @throws RequestException
     */
    public function uploadFile(Request $request)
    {
        $user = User::uploadFile($request);

        return $user;
    }

    /**
     * @return null
     */
    public function logout()
    {
        \Auth::logout();

        return redirect('/');
    }

    /**
     * @return false|string
     */
    public function isAuth()
    {
        return json_encode(!empty(\Auth::user()));
    }

    /**
     * @return false|string
     */
    public function getQuestions()
    {
        $questions = User::getQuestions();

        return json_encode($questions);
    }

    public function saveAnswer(Request $request)
    {
        $questions = Question::saveAnswer($request);

        return json_encode($questions);
    }

}
