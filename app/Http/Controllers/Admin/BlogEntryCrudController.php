<?php

namespace App\Http\Controllers\Admin;

use App\Models\BlogCategory;
use Backpack\CRUD\app\Http\Controllers\CrudController;

// VALIDATION: change the requests to match your own file names if you need form validation
use App\Http\Requests\BlogEntryRequest as StoreRequest;
use App\Http\Requests\BlogEntryRequest as UpdateRequest;
use Backpack\CRUD\CrudPanel;

/**
 * Class BlogEntryCrudController
 * @package App\Http\Controllers\Admin
 * @property-read CrudPanel $crud
 */
class BlogEntryCrudController extends CrudController
{
    public function setup()
    {
        /*
        |--------------------------------------------------------------------------
        | CrudPanel Basic Information
        |--------------------------------------------------------------------------
        */
        $this->crud->setModel('App\Models\BlogEntry');
        $this->crud->setRoute(config('backpack.base.route_prefix') . '/blogentry');
        $this->crud->setEntityNameStrings('blogentry', 'blog_entries');

        /*
        |--------------------------------------------------------------------------
        | CrudPanel Configuration
        |--------------------------------------------------------------------------
        */

        // TODO: remove setFromDb() and manually define Fields and Columns
        $this->crud->setFromDb();

        $categories = BlogCategory::getCategoriesForSelect();

        $this->crud->addField([
            'name' => 'blog',
            'label' => 'Категория',
            'type' => 'select_from_array',
            'options' => $categories,
        ]);

        $this->crud->removeField('image');
        $this->crud->addField([
            'name' => 'image', // The db column name
            'label' => 'Image', // Table column heading
            'type' => 'image',
        ]);
        $this->crud->removeField('publish_after');
        $this->crud->addField([
            'name' => 'publish_after', // The db column name
            'label' => 'Publish After', // Table column heading
            'type' => 'datetime_picker',
            // optional:
            'datetime_picker_options' => [
                'format' => 'DD/MM/YYYY HH:mm',
                'language' => 'en'
            ]
        ]);
        $this->crud->removeField('meta_tags');
        $this->crud->removeField('display_full_content_in_feed');

        $this->crud->removeField('content');
        $this->crud->addField([
            'name' => 'content',
            'label' => 'Content',
            'type' => 'tinymce'
        ]);
        $this->crud->addField([
            'label' => 'Страница',
            'type' => 'select',
            'name' => 'page_id',
            'entity' => 'page',
            'attribute' => 'name',
            'model' => 'App\Models\Page'
        ]);
        // add asterisk for fields that are required in BlogEntryRequest
        $this->crud->setRequiredFields(StoreRequest::class, 'create');
        $this->crud->setRequiredFields(UpdateRequest::class, 'edit');
    }

    public function store(StoreRequest $request)
    {
        // your additional operations before save here
        $redirect_location = parent::storeCrud($request);
        // your additional operations after save here
        // use $this->data['entry'] or $this->crud->entry
        return $redirect_location;
    }

    public function update(UpdateRequest $request)
    {
        // your additional operations before save here
        $redirect_location = parent::updateCrud($request);
        // your additional operations after save here
        // use $this->data['entry'] or $this->crud->entry
        return $redirect_location;
    }
}
