<?php

namespace App\Http\Controllers;

use App\Models\BlogEntry;
use App\Models\Faq;
use App\Models\Review;
use Bjuppa\LaravelBlog\Http\Controllers\BaseBlogController;
use Illuminate\Http\Request;

/**
 * Class CommonController
 * @package App\Http\Controllers
 */
class CommonController extends BaseBlogController
{
    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function getFaqs(Request $request)
    {
        $page = $request->page;
        $faqs = Faq::whereHas('page', function($query) use ($page) {
            $query->where('name', $page);
        })->get();

        return response()->json($faqs);
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function getReviews(Request $request)
    {
        $page = $request->page;
        $reviews = Review::whereHas('page', function($query) use ($page) {
            $query->where('name', $page);
        })->get();

        return response()->json($reviews);
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function getBlogs(Request $request)
    {
        $page = $request->page;
        $blogs = BlogEntry::whereHas('page', function($query) use ($page) {
            $query->where('name', $page);
        })->get();

        return response()->json($blogs);
    }

}
