<?php

namespace App\Http\Controllers;

use App\Exceptions\Models\RequestException;
use App\Models\BlogEntry;
use App\Models\Request as RequestModel;
use App\Models\Review;
use App\User;
use Carbon\Carbon;
use Facades\Bjuppa\LaravelBlog\Contracts\BlogRegistry;
use Illuminate\Http\Request;

/**
 * Class NewBlogController
 * @package App\Http\Controllers
 */
class NewBlogController extends Controller
{
    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function listBlogs()
    {
        return view('blog.list');
    }

    /**
     * @return \Illuminate\Http\JsonResponse
     */
    public function getPosts()
    {
        $blogs = BlogEntry::all();

        return response()->json($blogs);
    }

    /**
     * @return \Illuminate\Http\JsonResponse
     */
    public function getLastPosts()
    {
        $posts = BlogEntry::orderBy('id', 'DESC')
            ->limit(3)
            ->get();
        $posts = $posts->toArray();
        shuffle($posts);

        return response()->json($posts);
    }
}
