<?php

namespace App\Http\Controllers;

use Bjuppa\LaravelBlog\Http\Controllers\BaseBlogController;
use Illuminate\Support\Facades\View;
use Bjuppa\LaravelBlog\Contracts\BlogEntry;
use Illuminate\Support\Facades\Gate;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

/**
 * Class BlogController
 * @package Bjuppa\LaravelBlog\Http\Controllers
 */
class BlogController extends BaseBlogController
{
    /**
     * @return mixed
     * @throws \Throwable
     * @throws string
     */
    public function viewPost($slug)
    {
        throw_unless($entry = $this->blog->findEntry($slug), NotFoundHttpException::class);
        throw_unless(
            $entry->isPublic() or Gate::allows($this->blog->getPreviewAbility(), $entry),
            NotFoundHttpException::class
        );
        /**
         * @var $entry BlogEntry
         */

        return View::first($this->blog->bladeViews('entry', $entry))->with('entry', $entry)->with('entries', $this->blog->latestEntries());
    }
}
