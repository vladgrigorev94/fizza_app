@section('title', $entry->getTitle())
@include($blog->bladeView('entry.header'))
<body class="single single-post postid-49 single-format-standard">
<div class="mobile-menu">
    <div class="mobile-menu__close"></div>
    <!-- /.mobile-menu__close -->
    <div class="header__personal">
        <a href="#" class="header-call">
            <img src="/img/design/ico/call-answer.svg" alt="img">
        </a>
        <a href="#" class="header-personal">
            <img src="/img/design/ico/user.svg" alt="img">
            Личный кабинет
        </a>
    </div>
    <div class="header__nav">
        <ul>
            <li><a href="{{url('/')}}">Займы бизнесу</a></li>
            <li><a href="{{url('loan')}}">Займы под залог</a></li>
            <li><a href="{{url('blog')}}">Блог</a></li>
        </ul>
    </div>
</div>
<!-- /.mobile-menu -->
<div class="header">
    <div class="container">
        <div class="header-wrap">
            <a href="{{url('/')}}">
                <div class="header__logo">
                    <img src="/img/design/logo.svg" alt="img">
                </div>
            </a>
            <!-- /.header__logo -->
            <div class="header__nav header__nav--lg">
                <ul>
                    <li><a href="{{url('/')}}">Займы бизнесу</a></li>
                    <li><a href="{{url('loan')}}">Займы под залог</a></li>
                    <li><a href="{{url('blog')}}">Блог</a></li>
                </ul>
            </div>
            <!-- /.header__nav -->
            <div class="header__personal header__personal--lg">
                <a href="#" class="header-call">
                    <img src="/img/design/ico/call-answer.svg" alt="img">
                </a>
                <!-- /.header-call -->
                <a href="#" class="header-personal">
                    <img src="/img/design/ico/user.svg" alt="img">
                    Личный кабинет
                </a>
                <!-- /.header-personal -->
            </div>
            <!-- /.header__personal -->
            <div class="header__menu-open">
                <span></span>
                <span></span>
                <span></span>
            </div>
        </div>
    </div>
</div>
<div class="container zerogrid">
    <div class="col-2-3" id="post-container">
        <div class="wrap-col">
            <div class="post">
                <div class="post-margin">
                    <h4>{{ $entry->getTitle() }}</h4>
                    <ul class="post-status">
                        <li><i class="fa fa-clock-o"></i>
                            <time
                                    datetime="{{ $blog->convertToBlogTimezone($entry->getPublished())->toAtomString() }}"
                                    lang="ru" dir="ltr" itemprop="datePublished">
                                {{ $entry->getPublished()->diffForHumans() }}
                            </time>
                        </li>
                        <li><i class="fa fa-folder-open-o"></i>
                            <a href="#" title="Смотреть все посты"
                               rel="category">
                                {{ $entry->getBlog() }}
                            </a>
                        </li>
                        <li><i class="fa fa-comment-o"></i>Без комментариев</li>
                    </ul>
                    <div class="clear"></div>
                </div>

                <div class="featured-image">
                    {{ $entry->getImage() }}
                    <div class="post-icon">
                    <span class="fa-stack fa-lg">
                      <i class="fa fa-circle fa-stack-2x"></i>
                      <i class="fa fa-pencil fa-stack-1x fa-inverse"></i>
                    </span>
                    </div>
                </div>

                <div class="post-margin">
                    {{ $entry->getSummary() }}
                    <div class="post-tags">
            <span class="fa-stack fa-lg">
               <i class="fa fa-circle fa-stack-2x"></i>
               <i class="fa fa-tags fa-stack-1x fa-inverse"></i>
            </span><a href="#" rel="tag">Color Schemes</a>, <a href="#" rel="tag">Gallery</a>, <a href="#" rel="tag">Images</a>,
                        <a href="#" rel="tag">Light</a>, <a href="#" rel="tag">Post</a>, <a href="#"
                                                                                            rel="tag">Slider</a>, <a
                                href="#" rel="tag">Standard</a></div>
                    <div class="clear"></div>            <!-- End Post Tags -->

                </div>

                <!-- Post Social -->
                <ul class="post-social">
                    <li><a href="#" target="_blank"><i class="fa fa-facebook"></i></a></li>

                    <li><a href="#" target="_blank"><i class="fa fa-twitter"></i></a></li>

                    <li><a href="#" target="_blank"><i class="fa fa-google-plus"></i></a></li>

                    <li><a href="#" target="_blank">
                            <i class="fa fa-linkedin"></i></a></li>
                </ul>
                <div class="clear"></div>
            </div>
            <div class="post">
                <div class="post-margin">
                    <div class="related-posts">
                        <div class="post-avatar">
                            <div class="avatar-frame"></div>
                            <img width="70" height="70" src="/img/one-more-beer-70x70.png"
                                 class="attachment-post-widget #"/></div>
                        <div class="related-posts-aligned">
                            <h6><a href="#">One More Beer</a></h6>
                            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Mauris sit amet auctor ligula.
                                Donec eu</p>
                            <div class="clear"></div>
                        </div>

                        <div class="clear"></div>
                    </div>
                    <div class="related-posts">
                        <div class="post-avatar">
                            <div class="avatar-frame"></div>
                            <img width="70" height="70" src="/img/Port_Harbor1-70x70.jpg"
                                 class="attachment-post-widget #"/></div>
                        <div class="related-posts-aligned">
                            <h6><a href="#">Port Harbor</a></h6>
                            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Mauris sit amet auctor ligula.
                                Donec eu</p>
                            <div class="clear"></div>
                        </div>
                        <div class="clear"></div>
                    </div>
                    <div class="related-posts">
                        <div class="post-avatar">
                            <div class="avatar-frame"></div>
                            <img width="70" height="70" src="/img/Timothy-J-Reynolds-2560x14401-70x70.jpg"
                                 class="attachment-post-widget #"/></div>
                        <div class="related-posts-aligned">
                            <h6><a href="#">Underground Volcano</a></h6>
                            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Mauris sit amet auctor ligula.
                                Donec eu</p>
                            <div class="clear"></div>
                        </div>
                        <div class="clear"></div>
                    </div>
                    <div class="clear"></div>
                </div>
            </div>
            <div class="comment-container">
                <h6 id="comment-header">No Comments, Be The First!</h6>
                <ul class="comment-list">
                </ul>
                <div class="commen-form">
                    <div id="respond" class="comment-respond">
                        <h3 id="reply-title" class="comment-reply-title">
                            <small><a rel="nofollow" id="cancel-comment-reply-link" href="#" style="display:none;">Cancel
                                    Reply</a></small>
                        </h3>
                        <form action="" method="post" id="comment-form-container" class="comment-form">
                            <p class="comment-notes"></p>                            <input type="text" name="author"
                                                                                            placeholder="Enter Name"
                                                                                            class="comment-name"/>
                            <input type="text" name="email" placeholder="Enter Email" class="comment-email"/>
                            <input type="text" name="url" placeholder="Enter URL" class="comment-url"/>
                            <textarea name="comment" placeholder="Enter Message Here"
                                      class="comment-text-area"></textarea>
                            <p class="form-allowed-tags"></p>
                            <p class="form-submit">
                                <input name="submit" type="submit" id="comment-submit" value="Send Comment"/>
                                <input type='hidden' name='comment_post_ID' value='49' id='comment_post_ID'/>
                                <input type='hidden' name='comment_parent' id='comment_parent' value='0'/>
                            </p>
                        </form>
                    </div><!-- #respond -->
                    <div class="clear"></div>
                    </form>
                </div>
            </div>
            <div class="clear"></div>
        </div>
    </div>
    <div class="col-1-3">
        <div class="wrap-col">
            <div class="widget-container">
                <h6 class="widget-title">Последние посты</h6>
                <ul class="widget-recent-posts">
                    <?php $entries = $blog->latestEntries() ?>
                    @if($entries->count())
                        @each($blog->bladeView('entry.latestPost'), $entries, 'entry')
                    @endif
                </ul>
                <div class="clear"></div>
            </div>
            <div class="widget-container"><h6 class="widget-title">Tag Cloud</h6>
                <div class="tagcloud"><a href='#' class='tag-link-12' title='1 topic' style='font-size: 8pt;'>Color
                        Schemes</a>
                    <a href='#gallery' class='tag-link-6' title='1 topic' style='font-size: 8pt;'>Gallery</a>
                    <a href='#images' class='tag-link-8' title='1 topic' style='font-size: 8pt;'>Images</a>
                    <a href='#light' class='tag-link-11' title='1 topic' style='font-size: 8pt;'>Light</a>
                    <a href='#post' class='tag-link-7' title='1 topic' style='font-size: 8pt;'>Post</a>
                    <a href='#slider' class='tag-link-9' title='1 topic' style='font-size: 8pt;'>Slider</a>
                    <a href='#standard' class='tag-link-10' title='1 topic' style='font-size: 8pt;'>Standard</a></div>
                <div class="clear"></div>
            </div>
            <div class="clear"></div>
        </div>
    </div>
    <div class="clear"></div>
</div>
@include($blog->bladeView('entry.footer'))
<callback :action="'create-request'"></callback>
</body>
</html>
