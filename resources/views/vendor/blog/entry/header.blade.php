<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>
        @yield(
       'title',
        'Займы для бизнеса онлайн. Бизнес кредит без залога - ФИЗЗА.РУ'
        )
    </title>
    <meta name="description"
          content="Бизнес займы онлайн от 2%. На исполнение контрактов. На пополнение оборотных средств. Под залог авто и недвижимости. Бизнес кредит. Быстро и легко получите деньги для компании">
    <meta name="keywords"
          content="займы для бизнеса, бизнес заем, бизнес кредит, кредит для бизнеса, займы на исполнение контрактов, под залог авто, кредит бизнес">

    <link rel="stylesheet" href="/css/assets.css?v=258760696">
    <link rel="stylesheet" href="/css/style.css?v=258760696">
    <link rel='stylesheet' id='reset-css' href='/css/reset.css' type='text/css' media='all'/>
    <link rel='stylesheet' id='superfish-css' href='/css/superfish.css' type='text/css' media='all'/>
    <link rel='stylesheet' id='fontawsome-css' href='/css/font-awsome//css/font-awesome.css' type='text/css' media='all'/>
    <link rel='stylesheet' id='orbit-css-css' href='/css/orbit.css' type='text/css' media='all'/>
    <link rel='stylesheet' id='style-css' href='/css/style-blog.css' type='text/css' media='all'/>
    <link rel='stylesheet' id='color-scheme-css' href='/css/color/green.css' type='text/css' media='all'/>
    <link rel="stylesheet" href="/css/zerogrid.css" type="text/css" media="screen">
    <link rel="stylesheet" href="/css/responsive.css" type="text/css" media="screen">
    <script type='text/javascript' src='/js/jquery-1.10.2.min.js'></script>
    <script type='text/javascript' src='/js/jquery-migrate.min.js'></script>
    <script type='text/javascript' src='/js/jquery.carouFredSel-6.2.1-packed.js'></script>
    <script type='text/javascript' src='/js/hoverIntent.js'></script>
    <script type='text/javascript' src='/js/superfish.js'></script>
    <script type='text/javascript' src='/js/orbit.min.js'></script>
    <script src="/js/css3-mediaqueries.js"></script>

    <style>
        .header {
            position: relative !important;
            z-index: 5;
            top: 0;
            right: 0;
            left: 0;
            width: 100%;
            padding: 45px 0;
            height: 175px;
        }

        .header-call {
            line-height: 50px;
            padding: 0px !important;
            width: 50px;
            height: 50px;
            margin-right: 10px;
            transition: .3s;
            text-align: center;
            border-radius: 25px;
            background-color: rgba(255, 255, 255, .1);
            box-shadow: 0 15px 30px rgba(42, 20, 19, .1);
            background-color: #ED7574 !important;
        }

        .header-personal {
            font-size: 15px;
            font-weight: 300;
            line-height: 50px;
            display: inline-block;
            width: 210px;
            height: 50px;
            transition: .2s;
            text-align: center;
            vertical-align: top;
            color: #fff;
            border-radius: 25px;
            background-color: rgba(255, 255, 255, .1);
            box-shadow: 0 15px 30px rgba(42, 20, 19, .1);
            background-color: #ED7574 !important;
        }
        @media (min-width: 1200px){
            .container {
                max-width: 1005px;
            }
        }
        .header__menu-open span {
            display: block;
            width: 100%;
            height: 3px;
            margin-bottom: 5px;
            background: black;
        }
    </style>
</head>
<br>
