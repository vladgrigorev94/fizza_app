<div class="spacing-30"></div>
<div class="footer">
    <div class="container">
        <div class="footer-main">
            <div class="footer__logo">
                <img src="/img/design/logo-footer.svg" alt="img">
            </div>
            <div class="footer__info">
                <ul>
                    <li><a href="#">Пользовательское соглашение</a></li>
                    <li><a href="#">Политика конфиденциальности</a></li>
                    <li><a href="#">Общие условия договора займа</a></li>
                </ul>
            </div>
            <div class="footer__contact footer-contact">
                <a href="tel:+74952666568" class="footer-contact__phone">+7 (495) 266-65-58</a><br>
                <a href="mailto:hello@fizza.ru" class="footer-contact__mail">hello@fizza.ru</a>
            </div>
            <div class="footer__callback">
                <a href="#">
                    Обратный звонок
                </a>
            </div>
        </div>
        <div class="footer__copyright">
            © 2017-2019, ООО «ФИЗЗА» ИНН 7703437703 Все права защищены. Деятельность сайта fizza.ru не подлежит
            лицензированию или регулированию какими-либо специализированными агентствами, организациями или
            государственными учреждениями. Данный интернет-ресурс носит исключительно информационно-справочный характер,
            и не является публичной офертой или предложением делать оферты, определяемых положениями ст. 435, 437
            Гражданского кодекса Российской Федерации. ООО «ФИЗЗА» использует файлы cookie с целью персонализации
            сервисов и повышения удобства пользования веб-сайтом. Cookie представляют собой небольшие файлы, содержащие
            информацию о предыдущих посещениях веб-сайта. Если вы не хотите использовать файлы cookie, измените
            настройки браузера.
            <a href="mailto:hello@fizza.ru">hello@fizza.ru</a> <a href="tel:+74952666568">+7 495 266-65-68</a>
        </div>
    </div>
</div>
<script src="/js/vendor/bootstrap.min.js"></script>
<script src="/js/assets.js?v=258760696"></script>
<script src="/js/main.js?v=258760696"></script>
<br>
