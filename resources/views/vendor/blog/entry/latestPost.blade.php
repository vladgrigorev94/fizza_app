<?php
use Carbon\Carbon;
?>
<li>
    <div class="post-image">
        <div class="post-mask"></div>
        <img width="70" height="70" src="{{ $entry->getImageSrc() }}"
             class="attachment-post-widget #"/></div>

    <h6><a href="{{ $blog->urlToEntry($entry) }}">{{ $entry->getTitle() }}</a></h6>
    <span>
       <?=Carbon::instance($entry->getPublished())->diffForHumans();?>
    </span>
    <div class="clear"></div>
</li>
<br>
