<li class="carousel-item">
    <div class="post-margin">
        <h6>
            <a href="{{ $blog->urlToEntry($entry) }}" itemprop="url">
                {{ $entry->getTitle() }}
            </a>
        </h6>
        <span><i class="fa fa-clock-o"></i>
            <time
                    datetime="{{ $blog->convertToBlogTimezone($entry->getPublished())->toAtomString() }}"
                    lang="ru" dir="ltr" itemprop="datePublished">
                    {{ $entry->getPublished()->diffForHumans() }}
                </time>
        </span>
    </div>
    <div class="featured-image">
        <img width="290" height="130" src="{{ $entry->getImageSrc() }}"/>
        <div class="post-icon">
                    <span class="fa-stack fa-lg">
                        <i class="fa fa-circle fa-stack-2x"></i>
                        <i class="fa fa-pencil fa-stack-1x fa-inverse"></i>
                    </span>
        </div>
    </div>
    <div class="post-margin">
        <p>
            {{ $entry->getSummary() }}
        </p>
    </div>
</li>