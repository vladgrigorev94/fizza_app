@extends('layouts.secondStep')
@section('title', 'Шаг 2')
@section('css')
    <link rel="stylesheet" href="/css/firststep/assets.css">
    <link rel="stylesheet" href="/css/firststep/style.css">
    <link rel="stylesheet" href="/css/firststep/modal-request.css">
    <link rel="stylesheet" href="/css/secondstep/style.css">
@endsection
@section('content')
    <div class="modal modal--request modal-request" id="modal-request">
        <step-steps>
        </step-steps>
    </div>
    <div class="resolution-marker"></div>
@endsection

@section('js')
    <script type='text/javascript' src='/js/vendor/jquery-3.2.1.min.js'></script>
    <script src="/js/vendor/bootstrap.min.js"></script>
    <script src="/js/firststep/assets.js"></script>
    <script src="/js/firststep/main.js"></script>
    <script src="/js/firststep/modal-request.js"></script>
@endsection
