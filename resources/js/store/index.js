import Vue from 'vue';
import Vuex from 'vuex';

Vue.use(Vuex);

export default new Vuex.Store({
    state: {
        dummies: []
    },
    mutations: {
        dummyMutation(state, data) {
            state.dummies.push(data);
        }
    },
    actions: {
        dummy() {
            console.log('dummy');
        },
        urltoFile: function ({dispatch, commit}, {url, name, mimeType}) {
            return (fetch(url)
                    .then(function (res) {
                        return res.arrayBuffer();
                    })
                    .then(function (buf) {
                        return new File([buf], name, {type: mimeType});
                    })
            );
        }
    }
});
