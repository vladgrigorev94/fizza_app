// const $ = require('jquery');
$( document ).ready(function() {

	$(".modal-request__step").click(function() {
		console.log('modal-request__step');
		if ( !( $(this).hasClass("modal-request__step--active")) ) {
		 	//$(".modal-request__step--active").removeClass("modal-request__step--active");
		  	//$(this).addClass("modal-request__step--active");
		}
	});


	function rangeLineLength(range) {
		var rangeValue = range.val();
		var rangeValueStr = parseInt(rangeValue).toLocaleString('en-US').replace(/,/gi, ' ');
		$( '#' + range.data("output") ).text(  rangeValueStr );
		var inputLine = range.parent().find(".range-input-line");
		var length = (range.width());
		var max = range.attr("max");
		var step = range.attr("step")
		var inputLineWidth = (rangeValue-step)/(max-step)*length - (25*(rangeValue-step)/max);
		var inputLineWidth = inputLineWidth/length * 100;
		inputLine.css({width: (Math.round(inputLineWidth)) + '%'});
	}

	$( ".range-request" ).each( function() {
		rangeLineLength( $(this) );
	});

	$( ".range-request" ).on('input',function() {
		rangeLineLength( $(this) );
	});

	$(window).resize(function(){
		$( ".range-request" ).each( function() {
			rangeLineLength( $(this) );
		})
	});

});
